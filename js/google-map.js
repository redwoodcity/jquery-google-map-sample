
(function ($) {

	var currentInfoWindow = null;

	$.gmap = function(el, options) {

		//地図を表示
		var mapDiv = document.getElementById($(el).attr( "id" ));
		
		var mapCanvas = new google.maps.Map(mapDiv, {
			zoom: 14,
			center: new google.maps.LatLng( 35.6896342,139.6921007 ),
			scrollwheel: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});


		//地図上にマーカーを配置していく
		var bounds = new google.maps.LatLngBounds();
		var marker, i, latlng;
		for (i in options["markerList"]) {
			//マーカーを作成
			marker = options["markerList"][i];
			latlng = new google.maps.LatLng(marker.latlng[0], marker.latlng[1]);
			bounds.extend(latlng);
			var marker = createMarker(
				mapCanvas, latlng, marker.name, marker.content
			);

		}
		//マーカーが全て収まるように地図の中心とズームを調整して表示
		mapCanvas.fitBounds(bounds);
	}
	
	function createMarker( map, latlng, title, content ) {
		
		//マーカーを作成
		var marker = new google.maps.Marker({
			position : latlng,
			map : map,
			title : title
		});

		//情報ウィンドウを作成
		var infoWnd = new google.maps.InfoWindow({
			content : "<div><p><strong>" + title + "</strong></p><p>" + content + "<br />" + "</p></div>"
		});

		//マーカーがクリックされたら、情報ウィンドウを表示
		google.maps.event.addListener(marker, "click", function(){
			//先に開いた情報ウィンドウがあれば、closeする
			if (currentInfoWindow) {
				currentInfoWindow.close();
			}
			
			infoWnd.open(map, marker);
			
			currentInfoWindow = infoWnd;
		});

		return marker;
	}

	$.fn.gmap = function(options) {
		if (options === undefined) { options = {}; }

		new $.gmap(this, options);
	};
})(jQuery);